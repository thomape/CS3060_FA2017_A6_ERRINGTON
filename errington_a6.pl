%% Problem One - Reverse a list

%% Main rule
reverse([H|T], RevList) :- 
	reverseTail(T, [H], RevList).

%% Base case
reverseTail([], Count, Count).

%% Recurse tail
reverseTail([H|T], Count, RevList) :-
	reverseTail(T, [H|Count], RevList).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Problem Two - Find smallest element in a list

%% Checks first two values and compares
checkSmall([X, Y], Value) :-
	X < Y -> Value = X;
	Value = Y, !.

%% Tail recruse to comapre values in tail to head
checkSmall([X, Y|Z], Value) :-
	X < Y -> checkSmall([X|Z], Value);
	checkSmall([Y|Z], Value).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Problem Three - Flight between two towns

%% Knowledge Base
nonStopFlight(pittsburgh, cleveland).
nonStopFlight(philadelphia, pittsburgh).
nonStopFlight(columbus, philadelphia).
nonStopFlight(sanFrancisco, columbus).
nonStopFlight(detroit, sanFrancisco).
nonStopFlight(toledo, detroit).
nonStopFlight(houston, toledo).

%% Rule
flight(X, Y) :- 
	nonStopFlight(X, Y).

%% Recursive call to check towns
flight(X, Y) :- 
	nonStopFlight(X, Z),
	flight(Z, Y).


min([Second_Last, Last], Result):-
    Second_Last < Last
 -> Result = Second_Last
 ;  Result = Last, !.

min([First, Second|Rest], Result):-
    First < Second
 -> min([First|Rest], Result)
 ;  min([Second|Rest], Result).