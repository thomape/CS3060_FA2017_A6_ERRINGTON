Thomas Errington
Assignment 6 - Prolog part 2
Prof. Green
4NOV17 11:59


** Used sublime to create and edit with prolog file with the appropriate build tools and plugins

** Used SWI-Prolog interactive shell to 'Consult' errington_a6.pl and run tests

** Commands used:
	** Problem One*********************
		reverse([1,2,3,4,5], What).
		reverse([cow, llama, shark, dino], What).

	** Problem Two******************************
		checkSmall([1,2],What).
		checkSmall([123,234,345,456],What).
		checkSmall([14564654,546345656456,23453452346547,3245267534756,24353452345],What).
		checkSmall([-2,2],What).
		checkSmall([-34,-2,2,34],What).

	** Problem Three****************************
		flight(pittsburgh,cleveland).
		flight(houston,toledo).
		flight(houston,neverneverland).
		flight(houston, columbus).
		flight(houston,detroit).
		flight(pittsburgh, columbus).
		flight(pittsburgh, toledo).
		flight(houston, pittsburgh).


** Example Output:

** Problem One test**************************************************

?- reverse([1,2,3,4,5], What).
What = [5, 4, 3, 2, 1].

?- reverse([cow, llama, shark, dino], What).
What = [dino, shark, llama, cow].

** Problem Two test*************************************************

?- checkSmall([1,2],What).
What = 1 .

?- checkSmall([123,234,345,456],What).
What = 123 .

?- checkSmall([14564654,546345656456,23453452346547,3245267534756,24353452345],What).
What = 14564654 

̀?- checkSmall([-2,2],What).
What = -2 .

?- checkSmall([-34,-2,2,34],What).
What = -34 

** Problem Three test********************************************

?- flight(pittsburgh,cleveland).
true .

?- flight(houston,toledo).
true .

?- flight(houston,neverneverland).
false.

?- flight(houston, columbus).
true .

?- flight(houston,detroit).
true .

?- flight(pittsburgh, columbus).
false.

?- flight(pittsburgh, toledo).
false.

?- flight(houston, pittsburgh).
true